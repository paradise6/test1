<?php

	class dbMysql {

		private $db_host = 'localhost';
		private $db_user = 'root';
		private $db_pass = '';
		private $db_name = 'test';

		public function connect() {
			$opt = array(
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
				PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
			);

			try {
				$db = new PDO('mysql:host='.$this->db_host.
					';dbname='.$this->db_name.';charset=utf8',
					$this->db_user,
					$this->db_pass,
					$opt);
			}
			catch (Exception $e) {
				die('Error: '.$e->getMessage());
			}

			return $db;
		}

	}