<?php


include_once('lib/db.php');

class myTest {
	private $db_connect;

	private function __contruct() {
	}
	public function init() {
		$dbMysqlConnect = new dbMysql();
		$this->db_connect = $dbMysqlConnect->connect();
	}

	/*
	 * 1. Необходимо создать таблицу в MySQL для хранения книги. Книга поступает построчно, тоесть входные данные - строки. Размер строки в среднем 3-5мб. Нужно избежать повторения, каждая строка должна быть уникальной. Напишите структуру БД (TABLE DDL) и код, который обеспечит запись данных.
	 */
	public function run_test_1() {
		/*
		 * Структура таблицы
		 */
		$sql = '
			CREATE TABLE IF NOT EXISTS book
			(
				row_id INT(11) NOT NULL AUTO_INCREMENT, 
				row_text MEDIUMTEXT, 
				row_hash VARCHAR(32), 
				PRIMARY KEY(row_id),
				INDEX (row_hash)
			)
			ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci
		';

		$query = $this->db_connect->prepare($sql);
		$query->execute();

		/*
		 * Запускаем цикл на вставку 10 строк
		 */
		for($i=0; $i<10; $i++) {
			/*
			 * Получаем случайную строку
			 */
			$row_book = $this->get_row_book();
			/*
			 * Получаем ее хеш
			 */
			$hash_row_book = $this->get_hash($row_book);

			/*
			 * Ищем по хешу наличие такой строки
			 */
			$sql = "SELECT COUNT(row_id) as counts FROM book WHERE row_hash='".$hash_row_book."'";
			$query = $this->db_connect->prepare($sql);
			$query->execute();
			$find_book_row = $query->fetchColumn();

			/*
			 * Если не найдено строки по хешу
			 */
			if((int)$find_book_row == 0) {
				/*
				 * Производим ее сохранение в таблицу
				 */
				$sql = "
					INSERT INTO book
					SET
						row_text='".$row_book."',
						row_hash='".$hash_row_book."'
				";
				$query = $this->db_connect->prepare($sql);
				$query->execute();
			}
		}
	}

	function get_row_book($length=0) {
		/*
		 * Если нет размера устанавливаем в размере 3-5КБ
		 */
		if(!$length) $length = 1024 * 1024 * rand(3, 5);

		$maybe_letter = "qwertyuiopasdfghjklzxcvbnm_1234567890QWERTYUIOPLKJHGFDSAZXCVBNM ";
		$ret = '';
		for($i=0; $i<$length; $i++) {
			$ret .= $maybe_letter[rand(0, strlen($maybe_letter)-1)];
		}
		return $ret;
	}

	function get_hash($row_book) {
		return md5($row_book);
	}


	/*
	* 2. В проекте есть таблица для логирования событий, в которую добавляется около 300 записей каждую минуту. Таблица довольно большая, поэтому каждый месяц данные из неё необходимо архивировать.
	* Задача перенести данные за последний месяц из рабочей таблицы в архивную и очистить рабочую, при этом не потеряв никакие данные.
	* Нельзя использовать partitions.
	* Таблица:
	* CREATE TABLE logs (
	* created_at DATETIME NOT NULL DEFAULT NOW(),
	* text TEXT,
	* INDEX logs_created_at (created_at)
	 */

	public function run_test_2() {
		// Создаем таблицы логов
		$this->generate_table_logs();

		// Генерация случайных логов
		$this->generate_random_logs();


		/*
		 * Перенос логов в архив
		 * Поставить выполнение 1 раз в месяц
		 * Для облегчения запроса MySQL можно перенести определение даты последнего дня предыдущего месяца на сторону PHP и в таком случае вместо DATE(DATE_SUB(NOW(), INTERVAL (DAYOFMONTH(NOW())+1) DAY)) подставлять конкретную дату
		 */

		/*
		 * Выполняем копирование в архив
		 */
		$sql = '
				INSERT INTO logs_archive
				SELECT * FROM logs
				WHERE 
					DATE(created_at)<=DATE(DATE_SUB(NOW(), INTERVAL (DAYOFMONTH(NOW())+1) DAY))
			';
		$query = $this->db_connect->prepare($sql);
		$query->execute();

		/*
		 * Удаляем из основного лога
		 */
		$sql = '
				DELETE FROM logs
				WHERE 
					DATE(created_at)<=DATE(DATE_SUB(NOW(), INTERVAL (DAYOFMONTH(NOW())+1) DAY))
			';
		$query = $this->db_connect->prepare($sql);
		$query->execute();
	}
	/*
	 * Создаем таблицы логов
	 */
	function generate_table_logs() {

		/*
		 * Структура таблиц
		 * INDEX по полю типа DATETIME с моей точки зрения дает больше негатива, т.к. разрастается обїем таблиц за счет размера индекса
		 */
		$sql = '
			CREATE TABLE IF NOT EXISTS {name_table}
			(
				created_at DATETIME NOT NULL DEFAULT NOW(),
				text TEXT,
				INDEX logs_created_at (created_at)
			)
			ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci
		';

		/*
		 * Создаем основную таблицу логирования
		 */
		$query = $this->db_connect->prepare(str_replace('{name_table}', 'logs', $sql));
		$query->execute();

		/*
		 * Создаем архивную таблицу логирования
		 */
		$query = $this->db_connect->prepare(str_replace('{name_table}', 'logs_archive', $sql));
		$query->execute();
	}
	/*
	 * Генерация случайных логов
	 */
	function generate_random_logs() {
		/*
		 * Делаем 10 запросов на вставку
		 */
		for($i=0; $i<10; $i++) {
			$values = array();
			/*
			 * Делаем N случайных данных для вставки
			 */
			for($j=0; $j<50; $j++) {
				$rand_date  = mktime(mt_rand(0, 23), mt_rand(0, 59), mt_rand(0, 59), date("m")-mt_rand(0, 11), date("d")-mt_rand(0, 30), date("Y")-mt_rand(0, 1));
				$values[] = "'".$this->get_row_book(mt_rand(32, 1024))."', '".date('Y-m-d H:i:s', $rand_date)."'";
			}
			$sql = "INSERT INTO logs (text, created_at) VALUES (".implode('), (', $values).")";
			$query = $this->db_connect->prepare($sql);
			$query->execute();
		}
	}
	/*
	 * 3. Имеется БД Redis. На PHP необходимо реализовать две функции: создание и авторизация пользователя с помощью данной базы. Требование: при создании пользователя нужно проверять зарегистрирован ли такой пользователь.
	 * Данные пользователя должны содержать логин, имя, почтовый адрес и т.п.
	 */
	public function run_test_3() {
	}

}

	$mTest = new myTest();
	$mTest->init();
	//$mTest->run_test_1();
	//$mTest->run_test_2();
	$mTest->run_test_3();
